# MY MODULE: A SIMPLE PYTHON MODULE

def myfunc(x):
    """
    A wonderful mathematical function.
    
    Inputs:
        x: float
    
    Outputs:
        * square of x
    """
    return x**2